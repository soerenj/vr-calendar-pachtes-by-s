## A) Patched Version of "Vacation Rental Calendar", Wordpress Plugin
based on ["Vacation Rental Calendar", by duanestrong v1.2.2  ](https://de.wordpress.org/plugins/vr-calendar/)
Licence GPL 2.0 and higher

### added features
* style of calendar (/-stroke on transitions days) ![gradient](https://gitlab.com/soerenj/vr-calendar-pachtes-by-s/raw/screenshots/screenshots/gradient_day.png)
* week start (read from general wordpress-settings)
* available days green (optional)
* caching feed
* translated month names ([fallback on a few devices to englisch](https://gitlab.com/soerenj/vr-calendar-pachtes-by-s/commit/02913dd8b8fb421e9daa88614f146b1c9b37e88d))
* show fallback link to external calendar (if loading failed)

* multiple calendar in single line:
  * example: `http://example.com/ical1.ics http://example.com/ical2.ics http://example.com/ical3.ics`
  * will be merged
  * shows partly-free days (optional)
  * not displayed anymore: click popup for admins (not supported on multiple calendar)
  * technical details
    * important: every single feed-url has to start with http(s):// (known Bug)
    * rarely: if you have one single file-url, with multiple Calenders: "BEGIN:END BEGIN:VCALENDAR"  musst be in one line (no line break) (known Bug)

![](https://gitlab.com/soerenj/vr-calendar-pachtes-by-s/raw/screenshots/screenshots/partly-days.png)

### technical features
* check url (a lower securtiy reason) / forward HTTP-Status code (known BUG: doesn't work with Draft-Pages)

#### Download
zip of [vr-calendar-pachtes-by-s-*master*](https://gitlab.com/soerenj/vr-calendar-pachtes-by-s/tree/releases) (no auto-updates, will follow automatically; bedder for test-uses) ; ((Patchfiles removed)) change also the Plugin-Name a little bit.

--------------------------
# B) There is another Version
### "four Color": special mode for: one house, two floors
look this [branch "colorfull" ](https://gitlab.com/soerenj/vr-calendar-pachtes-by-s/tree/colorfull).
### add features
* addational color, for 1st or 2nd Floor

  `example.com/1st-foor.ics example.com/ical2.ics example.com/2nd-foor.ics`
* with a main-calendar for house
  `example.com/complete-house.ics example.com/1st-foor.ics example.com/ical2.ics example.com/2nd-foor.ics`

   ![](https://gitlab.com/soerenj/vr-calendar-pachtes-by-s/raw/screenshots/screenshots/four-color-detail.png)

![](https://gitlab.com/soerenj/vr-calendar-pachtes-by-s/raw/screenshots/screenshots/admin_four-color.png)

#### Download
zip of [vr-calendar-pachtes-by-s-*fourth-Color*](https://gitlab.com/soerenj/vr-calendar-pachtes-by-s/tree/releases) (no auto-updates, will follow automatically; bedder for test-uses) ; ((Patchfiles removed)) change also the Plugin-Name a little bit.

-------------------------
-------------------------
# Tests
* Responive Table with CSS Display:flex (browser must be older then ~2013/~2014) [deleted_commit](https://gitlab.com/soerenj/vr-calendar-pachtes-by-s/commit/6390962d169b676ea77baebc50a405d9e0dae161)
