<?php


// Register a new shortcode: [vrcalendar size="xxx"]
add_shortcode( 'vrcalendar', 'vr_calendar_shortcode' );

// The callback function that will replace [books]
function vr_calendar_shortcode( $attr ) {
	// Attributes
	$atts = shortcode_atts(
		array(
			'ical_url' => '',
			'size' => ''
		),
		$atts
	);

	$options = get_option( 'vr_calendar_options' );
	$ical_url = $options['ical_url'];
	
    //on self-Host request, dont break https (for most users unrelevant)
	$host   = parse_url($ical_url, PHP_URL_HOST);
	$scheme = parse_url($ical_url, PHP_URL_SCHEME);
	if ( $host==$_SERVER['HTTP_HOST'] &&
		 $_SERVER['HTTPS']!='' && $scheme=='http' ) $ical_url ='https://'.substr($ical_url,7);
		 
	$show_end_day = 'false';
	if( isset( $options['end_day_available'] ) )
	{
		if( ! $options['end_day_available'] )
		{
			$show_end_day = 'true';
		}
	}

  $weekStart = 0;
	if( get_option( 'start_of_week' ) != '' )
	{
		$weekStart = get_option( 'start_of_week' );
	}


	$partly_highlight = 'false';
	if( isset( $options['multi_cal_partly_highlight'] ) )
	{
		if( $options['multi_cal_partly_highlight'] )
		{
			$partly_highlight = 'true';
		}
	}

	
	
	$plugin_path = plugins_url() . '/vr-calendar';
	
	// css
    switch( $attr['size'] ) {
        case 'small':
        	$cal_style = file_get_contents( plugin_dir_path( __FILE__ ) . '../css/availabilityCalendarSmall.css');
            break;
        case 'medium':
			$cal_style = file_get_contents( plugin_dir_path( __FILE__ ) . '../css/availabilityCalendarMedium.css');
            break;
        default:
        case 'large':
        	$cal_style = file_get_contents( plugin_dir_path( __FILE__ ) . '../css/availabilityCalendarLarge.css');
            break;
    }
	
	// css gradient
	$cal_style .= file_get_contents( plugin_dir_path( __FILE__ ) . '../css/availabilityCalendarGradient.css');

	// URL override?
    if( isset( $attr['ical_url'] ) ) {
 		$ical_url = $attr['ical_url'];
 	}
 	
 	// admin view?
 	if( current_user_can( 'edit_pages' ) ) {
		$admin_view = 'true';
	}
	else {
		$admin_view = 'false';
	}
	


	// read from cache
	//if(isset($_GET['test']))echo $ical_url;
	$transient=md5(urldecode($ical_url)); 
	$options = get_option( 'vr_calendar_options' );
	$cachedValue = '';
	if( isset($options['cache']) && $options['cache']>0){
	  $cachedValue = get_transient( $transient);
	  if($cachedValue!==false){ $cachedValue=	preg_replace("/\r\n|\r|\n/","Mdef4YRNRNRREPALCE",addslashes( strip_tags($cachedValue) )); }
    }

	
		//prepare a few outputs (partly color explain)
	$availText = 'Available Night';
	$unavailText = 'Unavailable Night';
	$partlyText = "Partly available";
	if($options['name_avail']!='') $availText = $options['name_avail'];
	if($options['name_unavail']!='') $unavailText = $options['name_unavail'];
	if($options['name_partly']!='') $partlyText = $options['name_partly'];
	$availNight   = "<span style='padding:10px' class='availablenight' >   $availText  </span>";
	$unavailNight = "<span style='padding:10px' class='unavailablenight' > $unavailText </span>";
	$partly = "<span style='padding:10px;display:none' class='unavailablenight unavailablenight_not_full' > $partlyText </span>";
	
	if($options['fallback_link']!=''){
		$fallback = "<b>Please follow the link: <a href='".$options['fallback_link']."' target='_blank'>alternativ Calendar</a>.</b>";
	}else $fallback="";
	if (isset($_GET['preview_nonce']) && isset($attr['ical_url']) && $attr['ical_url']!='' ) $fallback.=" <br>Calendar load error: possible failed just in draft/preview mode";
	
    return "
    	<style> $cal_style </style>
		
		<p style='display:none;font-size: 16pt;margin-bottom: 30pt;' id='availablitycalendar_no_events_note' >$fallback</p>
		<noscript style='display:none;'>This calendar needs Javascript<br>$fallback</noscript>

     	<p id='calendar_pre_label'> $availNight $partly $unavailNight </p>
  		<div id='calendar' class='vrcalendar'>
   			<!-- availabilityCalendar.show() fills this in -->
  		</div>
 
		<!-- calendar AJAX data parser/loader --> 
 		<script language='JavaScript' type='text/javascript' src='$plugin_path/js/ajaxnet.js'> </script>
 		<script language='JavaScript' type='text/javascript' src='$plugin_path/js/icalparse.js'> </script>  
  
 		<!-- availability calendar -->  
 		<script language='JavaScript' type='text/javascript' src='$plugin_path/js/availabilitycalendar.js'> </script>
 		<script language='JavaScript' type='text/javascript'>
 				
		availabilityCalendar.rootNodeId = 'calendar';
		availabilityCalendar.popupNodeId = 'availabilityPopup';
		availabilityCalendar.ownerView = $admin_view;
		availabilityCalendar.showEndDay = $show_end_day;
		availabilityCalendar.weekStart = $weekStart;
		availabilityCalendar.partlyHighlight = $partly_highlight;

		var cachedValue = '$cachedValue';
		var icalUrl = '$plugin_path/php/icalfwd.php';
    if(cachedValue=='')
		  var icalLoader = new net.ContentLoader(icalUrl, avCaliCalLoaded, avCaliCalError,'POST','url=' + '$ical_url');
		else{
			var a =	new Object(  );

			a.ContentLoader=function(cachedValue,onload){
			  this.req = new Object();
			  this.req.responseText = cachedValue;
			  this.onload= onload;
			  this.onload.call(this);
			} 
			
      a.ContentLoader(cachedValue.replace(/Mdef4YRNRNRREPALCE/g,'\\n'), avCaliCalLoaded);
      
		}
		</script>
		
		<form name='availabilityPopup'
		      id='availabilityPopup'
		      class=popup
		      style='display:none'
		      enctype='multipart/form-data'
		>
		<table border=0>
		 <tr>
		  <td>Name</td>
		  <td> <input type='text' readonly name='summary' size=30  > </td>
		 </tr>
		 <tr>
		  <td>Arrive</td>
		  <td> <input type='text' readonly name='arrive' size=30  > </td>
		 </tr>
		 <tr>
		  <td>Depart</td>
		  <td> <input type='text' readonly name='depart' size=30  > </td>
		 </tr>
		 <tr>
		  <td> <input class=button type='button' value='Ok' onClick='document.availabilityPopup.style.display=\"none\";'>
		 </tr>
		</table>
		</form>	
	  	";
}
