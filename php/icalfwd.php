<?php

/* Copyright (c) 2014 Strong Engineering LLC */

if( $_SERVER['REQUEST_METHOD'] == 'GET'  )
{
	if( isset( $_GET['url'] ) )
	{
		$url = $_GET['url'];
		echo get_results($url);
	}
	
}

if( $_SERVER['REQUEST_METHOD'] == 'POST'  )
{
	if( isset( $_POST['url'] ) )
	{
		$url = $_POST['url'];
		echo get_results($url);	
	}
	
}

function get_results($url){
	
		require_once( '../../../../wp-load.php' ); //alternative register in vr-calendar.php
		
		// read from cache
		$transient=md5($url); 
		$options = get_option( 'vr_calendar_options' );
		if( isset($options['cache']) && $options['cache']>0){
		  $value = get_transient( $transient);
		  if($value!==false && $value!=''){ echo $value; exit; }
    }
    
    // new download
		$out = '';
		check_url($url);
		//readfile( $url );	
		$e = explode(" ", $url);
		foreach($e as $sub_url){
			$r = new HTTPRequest($sub_url);
			$out .= $r->DownloadToString();		
		}
		
		// save in cache
		$expiration=(int)$options['cache'];
		if( isset($options['cache']) && $options['cache']>0)
		  set_transient( $transient, $out, $expiration );
	   
		return $out;
}


//check the URL (prevent missuse your server like a proxy)
function check_url($url)
{
		require_once( '../../../../wp-load.php' ); //alternative register in vr-calendar.php
		$options = get_option( 'vr_calendar_options' );
	    $ical_url = $options['ical_url'];

		//compare with stored url
	    $found=false;
		if( $url==urldecode($ical_url) ) return;
	    if(substr($url,0,8)=='https://')
		  if( str_replace('https://','http://',$url)==urldecode($ical_url) ) return;
	
	    $query = new WP_Query( array( 's' => 'ical_url="'.$url.'"' ) );
	    if( $query->found_posts>0 )return;
	    
		if(substr($url,0,8)=='https://'){
	      $query = new WP_Query( array( 's' => 'ical_url="'.str_replace('https://','http://',$url).'"' ) );
		  if( $query->found_posts>0 ) return;
		}
	
	    header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request ', true, 400);
		die('400 Bad Request (this url is not allowed)');
}

// Have to fetch a HTTP URL the hard way. Supports "Location:"-redirections. Useful for servers with allow_url_fopen=false. Works with SSL-secured hosts. 
class HTTPRequest
{
    var $_fp;        // HTTP socket
    var $_url;        // full URL
    var $_host;        // HTTP host
    var $_protocol;    // protocol (HTTP/HTTPS)
    var $_uri;        // request URI
    var $_port;        // port
   
    // scan url
    function _scan_url()
    {
        $req = $this->_url;
       
        $pos = strpos($req, '://');
        $this->_protocol = strtolower(substr($req, 0, $pos));
       
        $req = substr($req, $pos+3);
        $pos = strpos($req, '/');
        if($pos === false)
            $pos = strlen($req);
        $host = substr($req, 0, $pos);
       
        if(strpos($host, ':') !== false)
        {
            list($this->_host, $this->_port) = explode(':', $host);
        }
        else
        {
            $this->_host = $host;
            $this->_port = ($this->_protocol == 'https') ? 443 : 80;
        }
       
        $this->_uri = substr($req, $pos);
        if($this->_uri == '')
            $this->_uri = '/';
    }
   
    // constructor
    function HTTPRequest($url)
    {
        $this->_url = $url;
        $this->_scan_url();
    }
   
    // download URL to string
    function DownloadToString()
    {
        $crlf = "\r\n";
       
        // generate request
        $req = 'GET ' . $this->_uri . ' HTTP/1.0' . $crlf
            .    'Host: ' . $this->_host . $crlf
         /*   .   'User-Agent: Mozilla/5.0' . $crlf */
         /*   .   'Content-type: text/calendar; charset=utf-8' . $crlf */
         /*   .   'Content-Disposition: inline; filename=calendar.ics' . $crlf  Google does not like this */
            .   'Accept: */*' . $crlf
            .    $crlf;

 
        // fetch
        $this->_fp = fsockopen(($this->_protocol == 'https' ? 'ssl://' : '') . $this->_host, $this->_port);
        fwrite($this->_fp, $req);
        while(is_resource($this->_fp) && $this->_fp && !feof($this->_fp))
            $response .= fread($this->_fp, 1024);
        fclose($this->_fp);
     
        // split header and body
        $pos = strpos($response, $crlf . $crlf);
        if($pos === false)
            return($response);
        $header = substr($response, 0, $pos);
        $body = substr($response, $pos + 2 * strlen($crlf));
       
        // parse headers
        $headers = array();
        $lines = explode($crlf, $header);
        foreach($lines as $line)
            if(($pos = strpos($line, ':')) !== false)
                $headers[strtolower(trim(substr($line, 0, $pos)))] = trim(substr($line, $pos+1));
       	
		// parse statuscode
		$line_first = substr($header,0,30);
		$status_code = (int) ( explode(" ", $line_first ) )[1];
		
		// check&forward statuscode
		if($status_code!=200 && !isset($headers['location']) )header($_SERVER['SERVER_PROTOCOL'] . ' '.$status_code.'  ', true, $status_code);
		
				
        // redirection?
        if(isset($headers['location']))
        {

            $http = new HTTPRequest($headers['location']);
            return($http->DownloadToString($http));
        }
        else
        {
            return(  /* $req . $header . */ $body );
        }
    }
} 
?>

