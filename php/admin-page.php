<?php

// Add a menu for our option page to admin Settings
add_action('admin_menu', 'vr_calendar_add_page');
function vr_calendar_add_page() {
	add_options_page( 
		'VR Calendar',				// page title
		'VR Calendar', 				// menu title
		'manage_options', 			// necessary capability
		'vr_calendar', 				// slug
		'vr_calendar_option_page' 	// draw function (below)
		);
}

// Draw the option page
function vr_calendar_option_page() {
	?>
	<div class="wrap">
		<h2>VR Calendar</h2>
		<form action="options.php" method="post">
			<?php settings_fields('vr_calendar_options'); ?>
			<?php do_settings_sections('vr_calendar'); ?>
			<input name="Submit" type="submit" value="Save Changes" />
		</form>
	</div>
	<?php
}

// Register and define the settings
add_action('admin_init', 'vr_calendar_admin_init');
function vr_calendar_admin_init(){
	// add data
	register_setting(
		'vr_calendar_options',			// group name
		'vr_calendar_options',			// get_option() name
		'vr_calendar_validate_options' 	// validate function (below)
	);
	// add display section
	add_settings_section(
		'vr_calendar_main',			// HTML id 
		'VR Calendar Settings',		// section title text
		'vr_calendar_section_text', // explanation text function (below)
		'vr_calendar'				// page to show on
	);
	// add form items
	add_settings_field(
		'vr_calendar_ical_feed',		// HTML id
		'Enter iCal feed URL here',		// info text
		'vr_calendar_setting_ical_feed', // draw function (below)
		'vr_calendar',					// page to show on
		'vr_calendar_main'				// page section HTML id
	);
	// add form items
	add_settings_field(
		'vr_calendar_end_day_available', // HTML id
		'Show end day as available',	// info text
		'vr_calendar_setting_end_day_available',	// draw function (below)
		'vr_calendar',					// page to show on
		'vr_calendar_main'				// page section HTML id
	);
	// add form items
	add_settings_field(
		'vr_calendar_cache',		// HTML id
		'Cache time',		// info text
		'vr_calendar_setting_cache', // draw function (below)
		'vr_calendar',					// page to show on
		'vr_calendar_main'				// page section HTML id
	);
	// add form items
	add_settings_field(
		'vr_calendar_multi_cal_partly_highlight', // HTML id
		'Show partly-free days<br><span style=\'font-weight:normal;font-size:8pt;color:#444444\'> just for <acronym title="you can generell insert multiple feed urls, seperated with white space." style="cursor:help;broder-bottom: 1pt dotted #444444">merged multi-calendars</acronym></span>',	// info text
		'vr_calendar_setting_multi_cal_partly_highlight',	// draw function (below)
		'vr_calendar',					// page to show on
		'vr_calendar_main'				// page section HTML id
	);	
	// add form items
	add_settings_field(
		'vr_calendar_setting_name_avail', // HTML id
		'Translate labels',	// info text
		'vr_calendar_setting_name_avail',	// draw function (below)
		'vr_calendar',					// page to show on
		'vr_calendar_main'				// page section HTML id
	);	
	// add form items
	add_settings_field(
		'vr_calendar_fallback_link',		// HTML id
		'Fallback Calendar-Website',		// info text
		'vr_calendar_settings_fallback_link', // draw function (below)
		'vr_calendar',					// page to show on
		'vr_calendar_main'				// page section HTML id
	);
}

// Draw the section header
function vr_calendar_section_text() {
	echo '<p>Your availability calendar will display on any page or post where you include the <b>[vrcalendar]</b> shortcode. You can specify a size
	like <b>[vrcalendar size=large]</b> where size can be small, medium, or large.</p><p>Your calendar is driven from a master calendar iCal feed that
	you enter below. This can be from any calendar that supports iCal like HomeAway, VRBO, FlipKey, AirBnB, Google, Apple, Microsoft, Yahoo and others.
	You can override this default URL on any page or post by adding the shortcode option ical_url like <b>[vrcalendar ical_url="http://activevacationrentals.com/tracker/ical/ical3.php"].</b></p>
    <p>
	Multiple Calendars:<br>
	<span style="padding-left:8pt;display:inline-block">
	<u>Seperate Calendars:</u> Create a page with &nbsp; [vrcalendar ical_url="http://example.com/ical<b>1</b>.ics] &nbsp; and &nbsp; [vrcalendar ical_url="http://example.com/ical<b>2</b>.ics]<br>
	<u>merged Calendars:</u> Insert two urls or add to page &nbsp; [vrcalendar ical_url="http://example.com/ical<b>1</b>.ics &nbsp; http://example.com/ical<b>2</b>.ics"] &nbsp; (important: every url must starts with http/https); optional you can give partly-availible days the color orange. 
	</span>
	</p>
	<p>Some calendar iCal feeds send the end date as the depart day, others send the end date as the last unavailable night. For those that send the end date as the depart day,
	clicking the "Show end day as available" box will strip off the last day so that it shows as an available night.</p>
<p>Error loading iCal file, after change the url? Try cleaning the Cache in Wordpress und in Browser, possible it helps (this failure due to security recheck the url, before ajax call; no other reason).</p>
	';
}

// Display and fill the form field for ical feed
function vr_calendar_setting_ical_feed() {
	// get option 'ical_url' value from the database
	$options = get_option( 'vr_calendar_options' );
	$ical_url = $options['ical_url'];
	// echo the field
	echo "<input id='ical_feed' size=60 name='vr_calendar_options[ical_url]' type='text' value='$ical_url' onkeypress=\"check_for_multiple_feeds(this)\" onpaste=\"check_for_multiple_feeds(this)\" oninput=\"check_for_multiple_feeds(this)\" onChange=\"check_for_multiple_feeds(this)\"/>";

	//just for as notice
	echo "<span id=\"ical_feed_note_multiple_feeds\" style=\"display:none;margin-left:20pt;transition: margin 4s;transition-timing-function: ease-in-out\"><br>&#128712; Are you inserting multiple feeds / calendars? Don't forget: every urls must start with http:// or https://</span>";
	echo "<script language=\"javascript\"  type='text/javascript'>
	function check_for_multiple_feeds(element){
	  var countEmpty = (element.value.trim().match(/ /g) || []).length;
	  var countHttp = (element.value.trim().match(/ https?:\/\//g) || []).length;
	  if( countEmpty>0 ){
		if(countEmpty>countHttp){
			document.getElementById('ical_feed_note_multiple_feeds').style.display='block';
			setTimeout(function(){ document.getElementById('ical_feed_note_multiple_feeds').style.marginLeft='0pt';},100);
		}else document.getElementById('ical_feed_note_multiple_feeds').style.display='none';
	  }else document.getElementById('ical_feed_note_multiple_feeds').style.display='none';
	}
	</script>";
}

// Display and fill the form field for end day
function vr_calendar_setting_end_day_available() {
	// get option 'end_day_available' value from the database
	$options = get_option( 'vr_calendar_options' );
	$end_day_available = $options['end_day_available'];
	// echo the field
	if( $end_day_available )
	{
		echo "<input id='end_day_available' name='vr_calendar_options[end_day_available]' type='checkbox' CHECKED >";
	}
	else
	{
		echo "<input id='end_day_available' name='vr_calendar_options[end_day_available]' type='checkbox' >";	
	}
}

// On multi Calendar, highlight partly free time
function vr_calendar_setting_multi_cal_partly_highlight() {
	$options = get_option( 'vr_calendar_options' );
	$multi_cal_partly_highlight = $options['multi_cal_partly_highlight'];
	// echo the field
	if( $multi_cal_partly_highlight )
	{
		echo "<input id='multi_cal_partly_highlight' name='vr_calendar_options[multi_cal_partly_highlight]' type='checkbox' CHECKED  value=''>";
	}
	else
	{
		echo "<input id='multi_cal_partly_highlight' name='vr_calendar_options[multi_cal_partly_highlight]' type='checkbox' value='1'>";	
	}

}

// Display and fill the form field for Cache time
function vr_calendar_setting_cache() {
	$options = get_option( 'vr_calendar_options' );
	$cache = $options['cache'];
	// echo the field
	echo "<input id='cache' size=30 name='vr_calendar_options[cache]' type='number' value='$cache' /> <br><i>in Seconds (3600=1hour); will be changed delayed (after old cache explired)</i>";
}

function vr_calendar_setting_name_avail() {
	$options = get_option( 'vr_calendar_options' );
	$name_avail = $options['name_avail'];
	$name_unavail = $options['name_unavail'];
	$name_partly = $options['name_partly'];

	echo "Available Night: &nbsp; &nbsp; <input id='name_avail' name='vr_calendar_options[name_avail]' type='text' value='".$name_avail."' /> <i></i><br>";	
	echo "Unavailable Night: &nbsp; &nbsp; <input id='name_unavail' name='vr_calendar_options[name_unavail]' type='text' value='".$name_unavail."' /> <i></i><br>";
	echo "Partly available Night: &nbsp; &nbsp; <input id='name_partly' name='vr_calendar_options[name_partly]' type='text' value='".$name_partly."' /> <i></i>";

}

// In case your Feed is offline or empty
function vr_calendar_settings_fallback_link(){
    // get option 'fallback_link' value from the database
	$options = get_option( 'vr_calendar_options' );
	$fallback_link = $options['fallback_link'];
	// echo the field
	echo "<input id='vr_calendar_fallback_link' size=60 name='vr_calendar_options[fallback_link]' type='text' value='$fallback_link' /><br><i>(optional) This url-link will be shown, if the iCal-Feed-Url failed (or is empty)</i>";
}

// Validate user input
function vr_calendar_validate_options( $input ) {

	$valid['ical_url'] = esc_url_raw( $input['ical_url'] );
	$valid['end_day_available'] = isset( $input['end_day_available'] );
	$valid['multi_cal_partly_highlight'] = isset( $input['multi_cal_partly_highlight'] ) ;
  $valid['cache'] = (int)( $input['cache'] ) ;
	$valid['name_avail'] = ( $input['name_avail'] ) ;
	$valid['name_unavail'] = ( $input['name_unavail'] ) ;
	$valid['name_partly'] = ( $input['name_partly'] ) ;
	$valid['fallback_link'] = esc_url_raw( $input['fallback_link'] ) ;
	
	return $valid;
}

